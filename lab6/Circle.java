public class Circle{
    int radius;
    Point center;
    public double area(){

    return(Math.PI*this.radius*this.radius);
    }
    public double perimeter(){
    return(2 * Math.PI* this.radius);
    }
    public boolean intersect(Circle c){
    	Point p = c.center;
    	Point p1 = this.center;
    	double a1 = (p.xCoord-p1.xCoord)*(p.xCoord-p1.xCoord);
    	double a2 =(p.yCoord-p1.yCoord)*(p.yCoord-p1.yCoord);
    	double a = Math.sqrt(a1+a2);
    
    	
    	if (a<this.radius+c.radius)
    		return true;
        return false;
    	
    }

    public static void main(String[] args){
	Point p = new Point(5,5);        
	Circle r = new Circle(5,p);	
	System.out.println(r.area());
	System.out.println(r.perimeter());
	System.outiprintln(intersect());
    }
}    
