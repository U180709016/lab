public class Rectangle{
    int sideA;
    int sideB;
    Point topLeft;
    public Rectangle(Point a ,int x ,int y){
    this.topLeft=a;    
    this.sideA=x;
    this.sideB=y;
    }
    public int area(){
    return(sideA*sideB);
    }
    public int perimeter(){
    return(2*(sideA+sideB));
    }
    public Point[] corners(){
    Point topRight=new Point(topLeft.xCoord+this.sideA,topLeft.yCoord);
    Point botLeft=new Point(topLeft.xCoord,topLeft.yCoord-this.sideB);
    Point botRight=new Point(botLeft.xCoord+this.sideA,topLeft.yCoord);
    Point[] array=new Point[4];
    array[0]=this.topLeft;
    array[1]=topRight;
    array[2]=botLeft;
    array[3]=botRight;
    return(array);
    }


    public static void main(String[]args){

    Point a = new Point(4,6);
    Rectangle r = new Rectangle(a,5,9);
    System.out.println(r.area());
    System.out.println(r.perimeter());
    Point[] points =r.corners();
    for(int m=0;m<points.length;m++)
 
        System.out.println(points[m].xCoord+""+points[m].yCoord);

    }



}

    
